/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modelo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Mile
 */
public class Ambos extends Usuario implements Serializable{
    private ArrayList<Venta> ventas;
    private ArrayList<Oferta> ofertas;
    private static final long serialVersionUID = 1234567890L;

    public Ambos(String rol, String nombres, String apellidos, String cedula, String correo, String organizacion, String usuario, String clave) {
        super(rol, nombres, apellidos, cedula, correo, organizacion, usuario, clave);
        this.ventas = new ArrayList<>();
        this.ofertas = new ArrayList<>();
    }
        
    //MÉTODOS QUE LE PERTENECES AL ROL VENDEDOR
    
     //Este método retorna falso si el vehículo que se ingreso no está registrado, si ya se encuentra 
     //registrado retorna verdadero 
    public static boolean validarPlaca(ArrayList<Vehiculo> vehiculos,String placa){
        for(Vehiculo v: vehiculos){
            if (v.getPlaca().equals(placa))
                return true;
        }
        return false;
    }
    
    //Este método acepta la oferta, manda el mail y agrega a la lista de ventas del usuario la nueva venta que se generó
    public void aceptarOferta(Oferta oferta){
        Venta venta= new Venta(oferta.getVehiculo(), this, oferta);
        venta.setVendido(true);
        String destinatario = oferta.getCorreo();
        String asunto = "SE HA ACEPTADO SU OFERTA";
        String cuerpo = venta.toString();
        Mail.enviarMail(destinatario, asunto, cuerpo);
        ventas.add(venta);
    }
    
    //Método que a partir de la lista de ventas que posee el usuario filtra los vehículos vendidos
    public ArrayList<Vehiculo> vehiculosVendidos(){
        ArrayList<Vehiculo> vendidos = new ArrayList<>();
        for(Venta venta: this.ventas){
            vendidos.add(venta.getVehiculo());
        }
        return vendidos;
    }
    
    //Método que rehace una lista con los vehículos que no se han vendido y descarta los que ya se vendieron
    public ArrayList<Vehiculo> eliminarVehiculo(ArrayList<Vehiculo> vehiculos){
        ArrayList<Vehiculo> sinVender = new ArrayList<>();
        ArrayList<Vehiculo> vendido = this.vehiculosVendidos();
        for (Vehiculo v: vehiculos){
            if(!vendido.contains(v))
                sinVender.add(v);
        }
        return sinVender;
    }
    
    //METODOS QUE LE PERTENCEN AL ROL COMPRADOR
    //Métodos que validan si es un entero o un double
    public static boolean convertirEntero(String numero){
        try{
            int entero = Integer.parseInt(numero);
            return true;
        }catch(Exception e){
            e.getStackTrace();
        }
        return false;
    }
    public static boolean convertirDouble(String numero){
        try{
            double entero = Double.parseDouble(numero);
            return true;
        }catch(Exception e){
            e.getStackTrace();
        }
        return false;
    }
    
    //
    public Oferta ofertar(Vehiculo vehiculo, double precioOfertado){
        Oferta oferta = new Oferta(this, vehiculo, precioOfertado);
        ofertas.add(oferta);
        return oferta;
    }
    
    
}
