/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modelo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author macbookpro
 */
public class Vendedor extends Usuario {    
    private Vehiculo v;
    private ArrayList<Venta> ventas;
 
    public Vendedor(String rol, String nombres, String apellidos, String cedula,String correo, String organizacion, String usuario, String clave) 
    {
        super(rol,nombres, apellidos, cedula,correo, organizacion, usuario, clave);    
         ventas = new ArrayList<>();
    }

    public Vendedor(Vehiculo v, ArrayList<Venta> ventas, String rol, String nombres, String apellidos, String cedula, String correo, String organizacion, String usuario, String clave) {
        super(rol, nombres, apellidos, cedula, correo, organizacion, usuario, clave);
        this.v = v;
        this.ventas = ventas;
    }
 
    
    // Retornará falso si la lista no contiene los atributos. 
    // Si contiene al menos uno --> retorna verdadero y ya no sería único.
    public boolean validarRegistro(String archivo)
    {
        ArrayList<String> atributos = new ArrayList<>();
        try(Scanner sc = new Scanner(new File(archivo)))
        {
            while(sc.hasNextLine())
            {
                String linea = sc.nextLine();
                String arr[] = linea.split(",");
                String correoRe = arr[2];
                String usuarioRe = arr[4];
                atributos.add(correoRe);
                atributos.add(usuarioRe);   
            }
        }
        catch(FileNotFoundException ex)
        {
            System.out.println("File not found");
        }
        
        return atributos.contains(this.usuario) || atributos.contains(this.correo);
    }
    
    public void registrarVendedor(String archivo)
    {
        try(FileWriter fw = new FileWriter(archivo,true);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter addTxt = new PrintWriter(bw))
        {
            addTxt.println(this.toString());
        }
        catch (Exception v)
        {
            System.out.println(v.getMessage());
        }
    }
    
    public boolean validarPlaca(String placa, String archivo)
    {
        ArrayList<String> placas = new ArrayList<>();
        try(Scanner sc = new Scanner(new File(archivo)))
        {
            while(sc.hasNextLine())
            {
                String linea = sc.nextLine();
                String arr[] = linea.split(",");
                String plac = arr[0];
                placas.add(plac);
                
            }
        }
        catch(FileNotFoundException ex)
        {
            System.out.println("No se encontró este archivo");
        }
        
        return placas.contains(placa);
        
    }
     
     // El vendedor llama a registrar vehiculo y envía el objeto obtenido en ingresaTipo().
     // Se guarda en una variable Vehiculo v = ingresaTipo().
     public void registrarVehiculo(Vehiculo o)
     {
         try(FileWriter fw = new FileWriter(o.getTipoVehiculo()+".txt",true);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter out = new PrintWriter(bw))
        {
            if(validarPlaca(o.getPlaca(),o.getTipoVehiculo()+".txt") == false)
                out.println(o.toString());
            else
                System.out.println("Ya existe esa placa registrada.");
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }  
     }
     
     public static void eliminarVehiculo(Venta venta)
     {
         String placa = venta.getVehiculo().getPlaca();
         String tipo = venta.getVehiculo().getTipoVehiculo();
         ArrayList<String> datos = new ArrayList<>();
         File tipos = new File(tipo+".txt");
         
         try(Scanner sc = new Scanner(tipos);
                 FileWriter fw = new FileWriter(tipo+".txt",true);
                 BufferedWriter bw = new BufferedWriter(fw);
                 PrintWriter out = new PrintWriter(bw))
                 
         {
             while(sc.hasNextLine())
             {
                 String linea = sc.nextLine();
                 String arr[] = linea.split(",");
                 String plak = arr[1];
                 if(Objects.equals(plak, placa))
                 {
                     datos.add(linea);
                 }
             }
             
             tipos.delete();
             for(String linea: datos)
             {
                 out.println(linea);
             }
                   
         }
         
         catch(Exception e)
         {
             System.out.println("No existe esa compra");
         }
     }

    public void aceptarOferta(Oferta oferta){
        Venta venta= new Venta(oferta.getVehiculo(), this, oferta);
        venta.setVendido(true);
        String destinatario = oferta.getCorreo();
        String asunto = "SE HA ACEPTADO SU OFERTA";
        String cuerpo = venta.toString();
        Mail.enviarMail(destinatario, asunto, cuerpo);
        ventas.add(venta);
    }
     
     
    @Override
    public String toString(){
         return nombres + "," + apellidos + "," + cedula + "," + correo + "," + organizacion + "," + usuario + "," + clave;
    }          
}