/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modelo;

import java.util.ArrayList;

/**
 *
 * @author Mile
 */
public class Camioneta extends Vehiculo{
    //ATRIBUTOS
    private int vidrios;
    
    //CONSTRUCTORES
    public Camioneta() {
    }
    public Camioneta(String tipoVehiculo, String placa, String marca, String modelo, String motor, int anio, double recorrido, String color, String combustible, String transmision, double precio, int vidrios) {
        super(tipoVehiculo, placa, marca, modelo, motor, anio, recorrido, color, combustible, transmision, precio);
        this.vidrios = vidrios;
    }

    public Camioneta(String tipoVehiculo, String placa, String marca, String modelo, String motor, int anio, double recorrido, String color, String combustible, String transmision, double precio,int vidrios, String pathImagen) {
        super(tipoVehiculo, placa, marca, modelo, motor, anio, recorrido, color, combustible, transmision, precio, pathImagen);
        this.vidrios = vidrios;
    }
    

    Camioneta(String tipo, String placa, String marca, String motor, int año, double recorrido, String color, String combustible, double precio, String transmision, int ventanas) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    //GETTERS
    public int getVidrios() {
        return vidrios;
    }

    @Override
    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    @Override
    public String getPlaca() {
        return placa;
    }

    @Override
    public String getMarca() {
        return marca;
    }

    /**
     *
     * @return
     */
    @Override
    public String getModelo() {
        return modelo;
    }

    @Override
    public String getMotor() {
        return motor;
    }

    @Override
    public int getAnio() {
        return anio;
    }

    @Override
    public double getRecorrido() {
        return recorrido;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getCombustible() {
        return combustible;
    }

    @Override
    public String getTransmision() {
        return transmision;
    }

    @Override
    public double getPrecio() {
        return precio;
    }

    @Override
    public ArrayList<Registros> getRegistros() {
        return registros;
    }

    @Override
    public String getPathImagen() {
        return pathImagen;
    }

    @Override
    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    @Override
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    @Override
    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Override
    public void setMotor(String motor) {
        this.motor = motor;
    }

    @Override
    public void setAnio(int anio) {
        this.anio = anio;
    }

    @Override
    public void setRecorrido(double recorrido) {
        this.recorrido = recorrido;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void setCombustible(String combustible) {
        this.combustible = combustible;
    }

    @Override
    public void setTransmision(String transmision) {
        this.transmision = transmision;
    }

    @Override
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public void setRegistros(ArrayList<Registros> registros) {
        this.registros = registros;
    }

    @Override
    public void setPathImagen(String pathImagen) {
        this.pathImagen = pathImagen;
    }
    
    //SETTERS
    public void setVidrios(int vidrios) {
        if (vidrios>=0)
            this.vidrios = vidrios;
    }
    
  
    public void imprimirVehiculo() {
        System.out.println("**** DATOS DEL VEHICULO ****");
        System.out.println("Tipo de Vehículo: " + this.tipoVehiculo + "\nPlaca: " + this.placa + "\nMarca: " + this.marca + "\nModelo: " + this.modelo + "\nMotor: " + this.motor + "\nAño: " + this.anio + "\nRecorrido: " + this.recorrido + "\nColor: " + this.color + "\nTipo de Combustible: " + this.combustible + "\nTrnasmisión: " + this.transmision + "\nNúmero de Vidrios: " + this.vidrios +"\nPrecio: " + this.precio);
    }
    
    //TOSTRING
    @Override
    public String toString() {
        return  tipoVehiculo + "," + placa + "," + marca + "," + modelo + "," + motor + "," + anio + "," + recorrido + "," + color + "," + combustible + "," + transmision + "," + vidrios + "," + precio + "," + this.pathImagen;
    }

     
}
