/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modelo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author Mile
 */
public class Oferta implements Serializable{
    //ATRIBUTOS
    private Vehiculo vehiculo;
    private String placaVehiculo;
    private String correo;
    private double precioOfertado;
    private static final long serialVersionUID = 1234567890L;
        
    //CONSTRUCTORES
    public Oferta() {
        
    }
    public Oferta(Usuario usuario, Vehiculo vehiculo, double precioOfertado) {
        this.placaVehiculo = vehiculo.getPlaca();
        this.correo = usuario.getCorreo();
        this.precioOfertado = precioOfertado;
        this.vehiculo = vehiculo;
    }
    public Oferta(Usuario usuario, String placa, double precioOfertado) {
        this.placaVehiculo = placa;
        this.correo = usuario.getCorreo();
        this.precioOfertado = precioOfertado;
    }
    //GETTERS
    public String getPlacaVehiculo() {
        return placaVehiculo;
    }
    public Vehiculo getVehiculo() {
        return vehiculo;
    }
    public double getPrecioOfertado() {
        return precioOfertado;
    }
    public String getCorreo() {
        return correo;
    }
    
    //SETTERS    **No se aplica Set del numero de Oferta ya que esta es única y no va a variar una vez que se genera
    public void setPlacaVehiculo(String placaVehiculo) {
        if (placaVehiculo!= null)
            this.placaVehiculo = placaVehiculo;
    }
    public void setVehiculo(Vehiculo vehiculo) {
        if (vehiculo!= null)
            this.vehiculo = vehiculo;
    }
    public void setPrecioOfertado(double precioOfertado) {
        if (precioOfertado>=0)
            this.precioOfertado = precioOfertado;
    }
    public void setCorreo(String correo) {
        this.correo = correo;
    } 
    public static ArrayList<Oferta> eliminarOferta(Oferta oferta, ArrayList<Oferta> ofertas){
        ArrayList<Oferta> noAceptadas = new ArrayList<>();
        for(Oferta o: ofertas){
            if (!o.equals(oferta)){
                noAceptadas.add(o);
            }
        }
        return noAceptadas;
    }
    
    public static void guardarOferta(ArrayList<Oferta> ofertas, String archivo){
        try(FileOutputStream f = new FileOutputStream(archivo);
            ObjectOutputStream out = new ObjectOutputStream(f);){
            out.writeObject(ofertas);
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    public static ArrayList<Oferta> leerOfertas(String archivo){
        try(FileInputStream f = new FileInputStream(archivo); ObjectInputStream in = new ObjectInputStream(f)){
            ArrayList<Oferta> ofertas = (ArrayList<Oferta>) in.readObject();
            return ofertas;
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return new ArrayList<>();
    }
    
    //EQUALS
    @Override
    public boolean equals(Object o) {
        if (o==null)        
            return false;
        if (this==o)        
            return true;
        if (this.getClass()!= o.getClass())    
            return false;
        
        Oferta other = (Oferta)o;
        return this.correo == other.correo && this.placaVehiculo==other.placaVehiculo && this.precioOfertado==other.precioOfertado;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.placaVehiculo);
        hash = 53 * hash + Objects.hashCode(this.correo);
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.precioOfertado) ^ (Double.doubleToLongBits(this.precioOfertado) >>> 32));
        return hash;
    }
    
    //TOSTRING
    @Override                                    
    public String toString() {
        return placaVehiculo + "," + this.correo + "," + precioOfertado;
    }
}
