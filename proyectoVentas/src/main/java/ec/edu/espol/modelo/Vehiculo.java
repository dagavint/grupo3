/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modelo;

import static ec.edu.espol.modelo.Ambos.convertirDouble;
import static ec.edu.espol.modelo.Ambos.convertirEntero;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author macbookpro
 */
public abstract class Vehiculo implements Serializable{ //implements Comparable<Vehiculo>
    //ATRIBUTOS
    protected String tipoVehiculo; 
    protected String placa;
    protected String marca;
    protected String modelo;
    protected String motor;  // Tipo de motor
    protected int anio;
    protected double recorrido;
    protected String color;
    protected String combustible;  //Tipo de combustible
    protected String transmision;
    protected double precio;
    protected ArrayList<Registros> registros;
    protected String pathImagen;
    
    //CONSTRUCTORES
    public Vehiculo() {
    }
    public Vehiculo(String tipoVehiculo, String placa, String marca, String modelo, String motor, int anio, double recorrido, String color, String combustible, String transmision, double precio) {
        this.tipoVehiculo = tipoVehiculo;
        this.placa = placa;
        this.marca = marca;
        this.modelo= modelo;
        this.motor = motor;
        this.anio = anio;
        this.recorrido = recorrido;
        this.color = color;
        this.combustible = combustible;
        this.transmision = transmision;
        this.precio = precio;
        this.registros=new ArrayList<>();
    }

    public Vehiculo(String tipoVehiculo, int anio, double recorrido, double precio) {
        this.tipoVehiculo = tipoVehiculo;
        this.anio = anio;
        this.recorrido = recorrido;
        this.precio = precio;
    }

    public Vehiculo(String tipoVehiculo, String placa, String marca, String modelo, String motor, int anio, double recorrido, String color, String combustible, String transmision, double precio, String pathImagen) {
        this.tipoVehiculo = tipoVehiculo;
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.motor = motor;
        this.anio = anio;
        this.recorrido = recorrido;
        this.color = color;
        this.combustible = combustible;
        this.transmision = transmision;
        this.precio = precio;
        this.pathImagen = pathImagen;
    }

  
    

    public ArrayList<Registros> getRegistros() {
        return registros;
    }
        
    //GETTERS
    public String getTipoVehiculo() {
        return tipoVehiculo;
    }
    public String getPlaca() {
        return placa;
    }
    public String getMarca() {
        return marca;
    }
    public String getModelo() {
        return modelo;
    }
    public String getMotor() {
        return motor;
    }
    public int getAnio() {
        return anio;
    }
    public double getRecorrido() {
        return recorrido;
    }
    public String getColor() {
        return color;
    }
    public String getCombustible() {
        return combustible;
    }
    public String getTransmision() {
        return transmision;
    }
    public double getPrecio() {
        return precio;
    }

    public String getPathImagen() {
        return pathImagen;
    }
    
    
    //SETTERS
    public void setTipoVehiculo(String tipoVehiculo) {
        if (tipoVehiculo!= null)
            this.tipoVehiculo = tipoVehiculo;
    }
    public void setPlaca(String placa) {
        if (placa!=null)
            this.placa = placa;
    }
    public void setMarca(String marca) {
        this.marca = marca;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    public void setMotor(String motor) {
        this.motor = motor;
    }
    public void setAnio(int anio) {
        if (anio>=1800)
            this.anio = anio;
    }
    public void setRecorrido(double recorrido) {
        if (recorrido>=0)
            this.recorrido = recorrido;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public void setCombustible(String combustible) {
        this.combustible = combustible;
    }
    public void setTransmision(String transmision) {
        this.transmision = transmision;
    }
    public void setPrecio(double precio) {
        if (precio>=0)
            this.precio = precio;
    }

    public void setRegistros(ArrayList<Registros> registros) {
        this.registros = registros;
    }

    public void setPathImagen(String pathImagen) {
        this.pathImagen = pathImagen;
    }
    
    public static ArrayList<String> filtrarTipos(ArrayList<Vehiculo> vehiculos){
        ArrayList<String> tipo = new ArrayList<>();
        for (Vehiculo v: vehiculos){
            if(!tipo.contains(v.getTipoVehiculo()))
                tipo.add(v.getTipoVehiculo());
        }
        return tipo;
    }
    
    public static ArrayList<Double> filtrarKm(ArrayList<Vehiculo> vehiculos){
        ArrayList<Double> recorrido = new ArrayList<>();
        for (Vehiculo v: vehiculos){
            if(!recorrido.contains(v.getRecorrido()))
                recorrido.add(v.getRecorrido());
        }
        return recorrido;
    }
    
    public static ArrayList<Double> filtrarPrecio(ArrayList<Vehiculo> vehiculos){
        ArrayList<Double> precio = new ArrayList<>();
        for (Vehiculo v: vehiculos){
            if(!precio.contains(v.getPrecio()))
                precio.add(v.getPrecio());
        }
        return precio;
    }
    
    public static ArrayList<Integer> filtrarYear(ArrayList<Vehiculo> vehiculos){
        ArrayList<Integer> year = new ArrayList<>();
        for (Vehiculo v: vehiculos){
            if(!year.contains(v.getAnio()))
                year.add(v.getAnio());
        }
        return year;
    }
    
    //Filtra los vehiculos a partir de los parámetros de busqueda
    public static ArrayList<Vehiculo> buscarVehiculos(ArrayList<Vehiculo> vehiculos, ArrayList<String> parametros){
        ArrayList<Vehiculo> filtrados = new ArrayList<>();
        String tipo = parametros.get(0);
        //Modularizar 
        double iRecorrido=Double.parseDouble(parametros.get(1));
        int iAnio=iAnio = Integer.parseInt(parametros.get(2));
        double iPrecio = Double.parseDouble(parametros.get(5));
        for (Vehiculo v: vehiculos){
            if(v.getTipoVehiculo().equals(tipo)&&(v.getRecorrido()==iRecorrido)&&(v.getAnio()==iAnio)&&(v.getPrecio()==iPrecio))
                filtrados.add(v);
        }
        return filtrados;
    }
    
    //EQUALS
    @Override    
    public boolean equals(Object o) {
        if (o==null)        
            return false;
        if (this==o)        
            return true;
        if (this.getClass()!= o.getClass())    
            return false;
        
        Vehiculo other = (Vehiculo)o;
        return Objects.equals(this.placa, other.placa);
    }  

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.placa);
        return hash;
    }
    
    public void registrarVehiculo(String archivo)
    {
        try(FileWriter fw = new FileWriter(archivo,true);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter addTxt = new PrintWriter(bw))
        {
            addTxt.println(this);
        }
        catch (Exception v)
        {
            System.out.println(v.getMessage());
        }
    } 
    public  static ArrayList<String> Buscar(String Archivo,ArrayList<String> a){           //[ motocicleta, 500, rojo]
        ArrayList<String> vehiculos= new ArrayList<>();
        try(Scanner sc= new Scanner(new File(Archivo))){
        while(sc.hasNext()){                
             double recInLista=Double.parseDouble(a.get(1));
             double recFinLista=Double.parseDouble(a.get(2));
             String linea= sc.nextLine();             
             String[] arr= linea.split(",");            
             String tipo=arr[0];
             String rec=arr[6];
             double recorrido=Double.parseDouble(rec);
             if (tipo.equals(a.get(0))&&(recInLista>=recorrido)&&(recFinLista<=recorrido)){
                 vehiculos.add(linea);
             }
         }   
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
     return vehiculos;
    }
    
     public static void guardarSerializados(ArrayList<Vehiculo> o, String nomb){
        try(FileOutputStream f= new FileOutputStream(nomb);
             ObjectOutputStream out= new ObjectOutputStream(f)){
            out.writeObject(o);
        }catch(FileNotFoundException e){
            System.out.println(e.getMessage());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
     public static ArrayList<Vehiculo> filtrarVehiculos(String tipo, ArrayList<Vehiculo> vehiculos){
        ArrayList<Vehiculo> tipoSelec= new ArrayList<>();
        for (Vehiculo v: vehiculos){
            if(v.getTipoVehiculo().equals(tipo)){
                tipoSelec.add(v);
            }
        }return tipoSelec;
    }
     
        public static ArrayList<Vehiculo> deserializarVehiculos(String archivo)
     {
         ArrayList<Vehiculo> lista = new ArrayList<>();
         try(FileInputStream fs = new FileInputStream(archivo);
             ObjectInputStream os = new ObjectInputStream(fs))
         {
             lista = (ArrayList<Vehiculo>)os.readObject();
             return lista;
         }
         catch(Exception e)
         {
             System.out.println("No hay archivo");
         }
         return lista;
     }
     
     public static ArrayList<String> tipoUnico(ArrayList<Vehiculo> vehiculos)
     {
         ArrayList<String> tipos = new ArrayList<>();
         for(Vehiculo v: vehiculos)
         {
             if(!tipos.contains(v.getTipoVehiculo())) tipos.add(v.getTipoVehiculo());
         }
         return tipos;
     }
    

    //TOSTRING
    @Override
    public String toString() {
        return this.tipoVehiculo+ ","+ this.color+ ","+ this.combustible+","+ this.marca+","+ this.recorrido+","+ this.modelo+","+ this.motor+","+this.anio+","+ this.placa+ ","+ this.precio+","+ this.transmision+ ","+ this.pathImagen;
    }   

    /*@Override
    public int compareTo(Vehiculo o) {
        
    }*/
}

