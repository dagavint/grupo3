/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.modelo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author macbookpro
 */
public class Usuario implements Serializable{
    protected String rol,nombres, apellidos, cedula, correo, organizacion, usuario, clave;
    private static final long serialVersionUID = 1234567890L;
    
    public Usuario(String rol,String nombres, String apellidos, String cedula, String correo, String organizacion, String usuario, String clave)
    {
        this.rol = rol;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.cedula=cedula;
        this.correo = correo;
        this.organizacion = organizacion;
        this.usuario = usuario;
        this.clave = clave;
    }
    
    public void imprimirUsuario(){
        System.out.println("**** DATOS DEL USUARIO ****");
        System.out.println("Nombres: "+nombres+"\nApellidos: "+apellidos+"\nC.I. "+cedula+"\nCorreo: "+correo+"\nOrdanización: "+organizacion);
    }
    

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCedula() {        //No se aplica setCedula ya que es una documentacion cuyo valor no varía.
        return cedula;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    
    public String getRol(){
        return rol;
    }
    
    public void setRol(String rol){
        this.rol = rol;
    }
    
    // La idea es usar los 3 metodos a continuacion para validar y registrar.
    // Por ejemplo, si quisiera registrar un nuevo usuario, llamaria al metodo validar
    // si retorna falso, deserealizo el archivo, agrego el nuevo usuario y finalmente serializo. fs
     public static void serializarUsuario(ArrayList<Usuario> u, String archivo)
     {
           try(FileOutputStream fs = new FileOutputStream(archivo);
                ObjectOutputStream os = new ObjectOutputStream(fs))
        {
            os.writeObject(u);
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
         
     }
     
     public static ArrayList<Usuario> deserializarUsuario(String archivo)
     {
         ArrayList<Usuario> u = new ArrayList<>();
         try(FileInputStream fs = new FileInputStream(archivo);
             ObjectInputStream os = new ObjectInputStream(fs))
         {
             u = (ArrayList<Usuario>)os.readObject();
             return u;
         }
         catch(Exception e)
         {
             System.out.println("No hay archivo");
         }
         return u;
     }
     
     // Valida el inicio de sesion
    // Los datos se los obtiene de los textfields 
    public static boolean validarIngreso(String archivo,String mail,String llave)
    {
        ArrayList<Usuario> listaU = deserializarUsuario(archivo);
        for(Usuario u: listaU)
        {
            String correo = u.getCorreo();
            String clave = u.getClave();
            if(Objects.equals(mail,correo) && Objects.equals(llave,clave))
                return true;
        }
        return false;
    }
    
    // Valida si existe o no ese usuario registrado con el dato obtenido del textfield
    public static boolean validarUsuarioUnico(String usuario, String archivo)
    {
        ArrayList<String> atributo = new ArrayList<>();
        ArrayList<Usuario> listaU = deserializarUsuario(archivo);
        for(Usuario u: listaU)
        {
            atributo.add(u.getCorreo());
        }
        return atributo.contains(usuario);
        
    }
    
    public static Usuario buscarUsuario(ArrayList<Usuario> usuarios, String mail, String llave){
       for(Usuario u: usuarios){
           if(Objects.equals(mail,u.getCorreo())&&Objects.equals(llave,u.getClave()))
               return u;
       }
       return null;
    }
    
    
    @Override
    public boolean equals(Object o) {
        if (o==null)        
            return false;
        if (this==o)        
            return true;
        if (this.getClass()!= o.getClass())    
            return false;
        
        Usuario other = (Usuario)o;
        return Objects.equals(this.cedula, other.cedula);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.cedula);
        return hash;
    }

    @Override
    public String toString() {
        return rol + "," + nombres + "," + apellidos + "," + cedula + "," + correo + "," + organizacion + "," + usuario + "," + clave;
    }
    
}
