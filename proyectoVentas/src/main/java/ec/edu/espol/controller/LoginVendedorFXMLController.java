/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.modelo.Ambos;
import ec.edu.espol.modelo.Vendedor;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author luis
 */
public class LoginVendedorFXMLController implements Initializable {
    
    Vendedor user;
    
    @FXML
    private Button salir;
   
    @FXML
    private Button Perfil;
    @FXML
    private Button Registrar;
    @FXML
    private Button Ofertas;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void irSalir(ActionEvent event) {
        try {
            App.setRoot("Principal");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void irPerfil(ActionEvent event) {
        //Pasar usuario
        try {
            FXMLLoader fxmlloader = App.loadFXMLload("PerfilFXML");
            App.setRoot(fxmlloader);
            PerfilFXMLController controller = fxmlloader.getController();
            controller.setUser(user);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void irRegistrar(ActionEvent event) {
        try {
            FXMLLoader fxmlloader = App.loadFXMLload("registrarVehiculoFXML");
            App.setRoot(fxmlloader);
            RegistrarVehiculoFXMLController controller = fxmlloader.getController();
            controller.setUser(user);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void irOfertas(ActionEvent event) {
        try {
            FXMLLoader fxmlloader = App.loadFXMLload("verOfertaFXML");
            App.setRoot(fxmlloader);
            VerOfertaFXMLController controller = fxmlloader.getController();
            controller.setUser(user);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void setUser(Vendedor u) {
        user = u;
    }
}
