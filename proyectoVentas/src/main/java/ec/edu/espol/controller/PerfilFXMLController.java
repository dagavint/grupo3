/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.modelo.Ambos;
import ec.edu.espol.modelo.Comprador;
import ec.edu.espol.modelo.Usuario;
import ec.edu.espol.modelo.Vendedor;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
/**
 * FXML Controller class
 *
 * @author macbookpro
 */
public class PerfilFXMLController implements Initializable {

    private Usuario user;
    @FXML
    private TextField txtInfoPerfil;
    @FXML
    private VBox vpanePerfil;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void cambiarCont(MouseEvent event) {
        vpanePerfil.getChildren().clear();
        
        // Creo los contenedores horizontales que contienen un texto y un campo de ingreso de texto
        HBox hbx1 = new HBox(new Text("Contraseña actual: "), new TextField());
        HBox hbx2 = new HBox(new Text("Contraseña nueva: "), new TextField());
        HBox hbx3 = new HBox(new Text("Confirme contraseña: "), new TextField());
        Button aceptar = new Button("Aceptar");
        
        // Agrego los contenedores horizontales a el contenedor vertical
        vpanePerfil.getChildren().addAll(hbx1,hbx2,hbx3,aceptar);
        
        aceptar.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                // Obtengo los datos ingresados en los campos de texto
                HBox datos1 = (HBox)vpanePerfil.getChildren().get(0);
                TextField txt1 = (TextField)datos1.getChildren().get(1);
                String clave_actual = txt1.getText();

                HBox datos2 = (HBox)vpanePerfil.getChildren().get(0);
                TextField txt2 = (TextField)datos2.getChildren().get(1);
                String clave_nueva = txt2.getText();

                HBox datos3 = (HBox)vpanePerfil.getChildren().get(0);
                TextField txt3 = (TextField)datos3.getChildren().get(1);
                String clave_nueva_conf = txt3.getText();

                // Validar que la clave nueva sea igual a la confirmacion

                if(!clave_nueva.equals(clave_nueva_conf))
                {
                    Alert a = new Alert(Alert.AlertType.INFORMATION,"La clave nueva no coincide con la confirmacion");
                    a.show();
                }
                else
                {
                    if(clave_actual.equals(user.getClave()))
                    {
                        user.setClave(clave_nueva);
                    }
                    else
                    {
                        Alert a = new Alert(Alert.AlertType.INFORMATION,"La clave actual no coincide con la registrada.");
                        a.show();
                    }
                } 
            }
        });
    }
    
    @FXML
    private void cambiarRol(MouseEvent event) {
        vpanePerfil.getChildren().clear();
        HBox hbx = new HBox(new Text("Nuevo rol: "), new TextField());
        Button aceptar = new Button("Aceptar");
        vpanePerfil.getChildren().addAll(hbx,aceptar);
        
        aceptar.setOnMouseClicked(new EventHandler<MouseEvent>(){
            
            @Override
            public void handle(MouseEvent t)
            {
                HBox datos = (HBox)vpanePerfil.getChildren().get(0);
                TextField txt = (TextField)datos.getChildren().get(1);
                String nuevo_rol = txt.getText();

                user.setRol(nuevo_rol);
            }
            
        });
        
    }

    @FXML
    private void regresar(MouseEvent event) {
        String rol = user.getRol();
        System.out.println(user);
        switch(rol){
            case "Vendedor":
                try {
                    FXMLLoader fxmlloaderV = App.loadFXMLload("loginVendedorFXML");
                    App.setRoot(fxmlloaderV);
                    LoginVendedorFXMLController controllerV = fxmlloaderV.getController();
                    controllerV.setUser((Vendedor)user);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Comprador":
                try{
                    FXMLLoader fxmlloaderC = App.loadFXMLload("BusquedaAutoComprador");
                    App.setRoot(fxmlloaderC);
                    BusquedaAutoCompradorController controllerC = fxmlloaderC.getController();
                    controllerC.setUser((Comprador)user);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Ambos":
                try {
                    FXMLLoader fxmlloaderA = App.loadFXMLload("LoginAmbos");
                    App.setRoot(fxmlloaderA);
                    LoginAmbosController controllerA = fxmlloaderA.getController();
                    controllerA.setUser((Ambos) user);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    public void setUser(Usuario u) {
        user = u;
        txtInfoPerfil.setText(u.toString());
    }

}
