/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.modelo.Ambos;
import ec.edu.espol.modelo.Auto;
import ec.edu.espol.modelo.Oferta;
import ec.edu.espol.modelo.Usuario;
import ec.edu.espol.modelo.Vehiculo;
import ec.edu.espol.modelo.Vendedor;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
/**
 * FXML Controller class
 *
 * @author macbookpro
 */
public class OfertarCompradorController implements Initializable {
    Usuario user;
    ArrayList<Oferta> ofertas;
    String placa;
    Oferta offer;
    
    @FXML
    private TextField txtVehiculoAOfertar;
    @FXML
    private TextField txtOferta;
    @FXML
    private TextField txtCorreo;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ofertas = Oferta.leerOfertas("ofertas.ser");
        VBox vbx = new VBox();
        for (Oferta o: ofertas){
        
        }
    }    
    
    @FXML
    private void ofertar(MouseEvent event) {
        boolean cond1 = txtOferta.getText().isEmpty()||txtCorreo.getText().isEmpty();
        boolean cond2 = validarCorreo(txtCorreo.getText())&&validarDecimal(txtOferta.getText());
        if (!cond1&&cond2){
        double oferta = Double.parseDouble(txtOferta.getText());
        offer = new Oferta(user, placa, oferta);
        ofertas.add(offer);
        Oferta.guardarOferta(ofertas, "ofertas.ser");
        }
        else{
            Alert a = new Alert(Alert.AlertType.ERROR,"Campos incorrectos");
            a.show();
        }
    }

    @FXML
    private void regresar(MouseEvent event) {
        if (user.getRol().equals("Comprador")){
            try {
                FXMLLoader fxmlloader = App.loadFXMLload("BusquedaAutoComprador");
                App.setRoot(fxmlloader);
                BusquedaAutoCompradorController controller = fxmlloader.getController();
                controller.setUser((Vendedor) user);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        else{
            try {
                FXMLLoader fxmlloader = App.loadFXMLload("LoginAmbos");
                App.setRoot(fxmlloader);
                LoginAmbosController controller = fxmlloader.getController();
                controller.setUser((Ambos)user);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void setUser(Usuario u,String atributos){
        user=u;
        txtVehiculoAOfertar.setText(atributos);
        placa = atributos.split(",")[1];
    }
    
    public static boolean validarCorreo(String email){
        String patron = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@" + "[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
        return email.matches(patron);
    }
    public static boolean validarDecimal(String oferta){
        String patron = "^[0-9]+([.][0-9]+)?$";
        return oferta.matches(patron);
    }
    
}
