/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.modelo.Ambos;
import ec.edu.espol.modelo.Usuario;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import javafx.scene.input.MouseEvent;
/**
 * FXML Controller class
 *
 * @author Mile
 */
public class LoginAmbosController implements Initializable {
    private Ambos user;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void perfil(MouseEvent event) {
        //Paso de datos
        try {
            FXMLLoader fxmlloader = App.loadFXMLload("PerfilFXML");
            App.setRoot(fxmlloader);
            PerfilFXMLController controller = fxmlloader.getController();
            controller.setUser(user);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void registrarVehiculo(MouseEvent event) {
        try {
            FXMLLoader fxmlloader = App.loadFXMLload("registrarVehiculoFXML");
            App.setRoot(fxmlloader);
            RegistrarVehiculoFXMLController controller = fxmlloader.getController();
            controller.setUser(user);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void aceptarOferta(MouseEvent event) {
        try {
            FXMLLoader fxmlloader = App.loadFXMLload("verOfertaFXML");
            App.setRoot(fxmlloader);
            VerOfertaFXMLController controller = fxmlloader.getController();
            controller.setUser(user);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void ofertar(MouseEvent event) {
        try {
            FXMLLoader fxmlloader = App.loadFXMLload("BusquedaAutoComprador");
            App.setRoot(fxmlloader);
            BusquedaAutoCompradorController controller = fxmlloader.getController();
            controller.setUser(user);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void salir(MouseEvent event) {
        try {
            App.setRoot("Principal");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }
    
    public void setUser(Ambos u){
        user = u;
    }

}
