/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.modelo.Ambos;
import ec.edu.espol.modelo.Comprador;
import ec.edu.espol.modelo.Encriptar;
import ec.edu.espol.modelo.Usuario;
import ec.edu.espol.modelo.Vendedor;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
;
/**
 * FXML Controller class
 *
 * @author Mile
 */
public class RegistrarController implements Initializable {

    private ArrayList<Usuario> usuarios;
    @FXML
    private RadioButton vendedor;
    @FXML
    private RadioButton comprador;
    @FXML
    private RadioButton ambos;
    @FXML
    private TextField tf1; //nombres
    @FXML
    private TextField tf2; //apellidos;
    @FXML
    private TextField tf3; //ci
    @FXML
    private TextField tf4; //email;
    @FXML
    private TextField tf5; //org
    @FXML
    private TextField tf6; //user
    @FXML
    private TextField tf7; //clave
    @FXML
    private Button reg;
    @FXML
    private Button back;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       usuarios = Usuario.deserializarUsuario("usuarios.ser");
       vendedor.setSelected(false);
       comprador.setSelected(false);
       ambos.setSelected(false);
       rbSelect(vendedor,comprador,ambos);
       rbSelect(comprador,vendedor,ambos);
       rbSelect(ambos,comprador,vendedor);
    }  
    
    @FXML
    private void registrar(MouseEvent event) {
        boolean cond1 = !(vendedor.isSelected()!=false || comprador.isSelected()!=false || ambos.isSelected()!=false);
        boolean cond2 = tf1.getText().isEmpty()||tf2.getText().isEmpty()||tf3.getText().isEmpty()||tf4.getText().isEmpty()||tf5.getText().isEmpty()||tf6.getText().isEmpty()||tf7.getText().isEmpty();
        boolean cond3 = validarNomApOrg(tf1.getText())&&validarNomApOrg(tf2.getText())&&validarCI(tf3.getText())&&validarCorreo(tf4.getText())&&validarNomApOrg(tf5.getText())&&validarFormatUser(tf6.getText())&&validarClave(tf7.getText());
        if(!(cond1||cond2)&&cond3){
            String rol = retornaRol(vendedor,comprador,ambos);
            vendedor.setSelected(false); comprador.setSelected(false); ambos.setSelected(false);
            String nombres = tf1.getText();
            tf1.clear();
            String apellidos= tf2.getText();
            tf2.clear();
            String ci=  tf3.getText();
            tf3.clear();
            String email= tf4.getText();
            tf4.clear();
            String org= tf5.getText();
            tf5.clear();
            String user= tf6.getText();
            tf6.clear();
            String clave = null;
            try{
                clave= Encriptar.encriptaHex(Encriptar.to256((String)tf7.getText()));
            }catch(NoSuchAlgorithmException e){
                e.getStackTrace();
            }
            tf7.clear();
            if (!Usuario.validarUsuarioUnico(email,"usuarios.ser")){
                switch(rol){
                    case "Vendedor":
                        usuarios.add(new Vendedor(rol,nombres,apellidos,ci,email,org,user,clave));
                        break;
                    case "Comprador":
                        usuarios.add(new Comprador(rol,nombres,apellidos,ci,email,org,user,clave));
                        break;
                    case "Ambos":
                        usuarios.add(new Ambos(rol,nombres,apellidos,ci,email,org,user,clave));
                        break;
                    default:
                        break;
                }
                Alert a = new Alert(Alert.AlertType.INFORMATION,"SE HA REGISTRADO EXITOSAMENTE");
                a.show();
            }
            else{
                Alert a = new Alert(Alert.AlertType.ERROR,"USUARIO YA EXISTENTE");
                a.show();
            }
            Usuario.serializarUsuario(usuarios,"usuarios.ser");
            System.out.println(usuarios);
        }
        else{
            Alert a = new Alert(Alert.AlertType.ERROR,"NO SE PUDO REALIZAR EL REGISTRO");
            a.show();
        }
    }
    
    private static String retornaRol(RadioButton c, RadioButton v, RadioButton a){
        if (c.isSelected())
            return c.getText();
        else if (v.isSelected())
            return v.getText();
        else 
            return a.getText();
    }
    
    @FXML
    private void regresar(MouseEvent event) {
        try {
            App.setRoot("Principal");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void rbSelect(RadioButton a, RadioButton b, RadioButton c){
        a.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent t) {
                if (a.isSelected()){
                    b.setSelected(false);
                    c.setSelected(false);
                }
            }
        });     
    }

    public static boolean validarCI(String ci){
	return ci.matches("^[0-9]{10}$");
    }

    public static boolean validarNomApOrg(String nombres){
        return nombres.matches("^([A-Z]{1}[a-z]+[ ]?){1,2}$");
    }
    // fuente: https://fluidattacks.com/web/defends/java/validar-correo-electronico/
    public static boolean validarCorreo(String email){
        String patron = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@" + "[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
        return email.matches(patron);
    }
    // fuente: http://w3.unpocodetodo.info/utiles/regex-ejemplos.php?type=psw
    public static boolean validarClave(String clave){
        String patron = "^(\\d?)([A-Z]?)([a-z]?)\\S{8,}$";
        return clave.matches(patron);
    }
    
    public static boolean validarFormatUser(String user){
        String patron = "^(\\d?)([A-Z]?)([a-z]?)\\S{5,16}$";
        return user.matches(patron);
    }
}
