/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.modelo.Ambos;
import ec.edu.espol.modelo.Oferta;
import ec.edu.espol.modelo.Usuario;
import ec.edu.espol.modelo.Vehiculo;
import ec.edu.espol.modelo.Vendedor;
import ec.edu.espol.modelo.Venta;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
//import modelo.Oferta;
/**
 * FXML Controller class
 *
 * @author luis
 */
public class VerOfertaFXMLController implements Initializable {

    Usuario user;
    
    @FXML
    private TextField placa;
    @FXML
    private Button buscar;
    @FXML
    private Button aceptar;
    @FXML
    private Button irAtras;
    @FXML
    private ScrollPane spPrincipal;
    @FXML
    private VBox vbPrincipal;
    @FXML
    private ToggleGroup grupoRadioBotones;
    @FXML
    private TextField correoSele;
    @FXML
    private ArrayList<Oferta> ofertas= Oferta.leerOfertas("ofertas.ser");
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void buscarOferta(ActionEvent event) {
        ArrayList<Oferta> ofertas= Oferta.leerOfertas("ofertas.ser");
        ofertas.sort((Oferta o1, Oferta o2)-> (int)(o2.getPrecioOfertado()- o1.getPrecioOfertado())); //Ordenar la lista antes de recorrerla para imprimirla
        for(Oferta o: ofertas){
            if(placa.getText().equals(o.getPlacaVehiculo())){                
                HBox hb= new HBox();                                               
                TextField oferta= new TextField("Precio Ofertado: "+o.getPrecioOfertado());                
                TextField correo= new TextField("Correo: "+o.getCorreo());
                RadioButton rd1= new RadioButton(o.getCorreo());
                rd1.setToggleGroup(grupoRadioBotones);
                hb.getChildren().add(rd1);
                hb.getChildren().add(oferta);
            }
        }
    }

    @FXML
    private void aceptarOferta(ActionEvent event) {
        RadioButton select= (RadioButton)grupoRadioBotones.getSelectedToggle();
        String correo= select.getText();
        Double precio = null;
        Vehiculo v = null;
        for(Oferta o: ofertas){
            if(placa.getText().equals(o.getPlacaVehiculo())){
                v=o.getVehiculo();
                precio=o.getPrecioOfertado();
            }      
        select.getText();
        }
        Oferta of= new Oferta(user,v,precio);
        Vendedor vend=(Vendedor) user;
        vend.aceptarOferta(of);        
        Venta venta= new Venta(v,vend,of);
        Vendedor.eliminarVehiculo(venta);
    }
    
    
    @FXML
    private StringBuilder leerRadioButton(RadioButton rb){
        StringBuilder sb= new StringBuilder();
        sb.append(rb.getText());
        String st=rb.getText();
        ArrayList<String> lee= new ArrayList<>();
        lee.add(st);
        return sb;
    }
    
    @FXML
    private void irAtras(ActionEvent event) {
        if (user.getRol().equals("Vendedor")){
            try {
                FXMLLoader fxmlloader = App.loadFXMLload("loginVendedorFXML");
                App.setRoot(fxmlloader);
                LoginVendedorFXMLController controller = fxmlloader.getController();
                controller.setUser((Vendedor) user);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        else{
            try {
                FXMLLoader fxmlloader = App.loadFXMLload("LoginAmbos");
                App.setRoot(fxmlloader);
                LoginAmbosController controller = fxmlloader.getController();
                controller.setUser((Ambos)user);
            } catch (IOException ex) {
                ex.printStackTrace();
                
            }
        } 
    }
    
    public void setUser(Usuario u){
        user = u;
    }

}
