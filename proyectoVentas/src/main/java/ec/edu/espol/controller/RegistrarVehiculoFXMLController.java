/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;
//import ec.edu.espol.controller.RegistrarVehiculoFXMLController;
import ec.edu.espol.gui.App;
import ec.edu.espol.modelo.Ambos;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;
import ec.edu.espol.modelo.Auto;
import ec.edu.espol.modelo.Camion;
import ec.edu.espol.modelo.Camioneta;
import ec.edu.espol.modelo.Motocicleta;
import ec.edu.espol.modelo.Usuario;
import ec.edu.espol.modelo.Vehiculo;
import ec.edu.espol.modelo.Vendedor;
import java.util.Objects;
import java.util.regex.Pattern;
import javafx.fxml.FXMLLoader;

public class RegistrarVehiculoFXMLController implements Initializable {
    Usuario user;
    @FXML
    private ComboBox cbtipo;
    @FXML
    private TextField placa;
    @FXML
    private TextField marca;
    @FXML
    private TextField modelo;
    @FXML
    private TextField motor;
    @FXML
    private TextField anio;
    @FXML
    private TextField color;
    @FXML
    private TextField combust;
    @FXML
    private TextField trans;
     @FXML
    private TextField recorrido;
    @FXML
    private TextField precio;
    @FXML
    private Button atras;
    @FXML
    private Button registrarV;
    @FXML
    private FileChooser fileChooser;
    @FXML
    private Window stage;
    @FXML
    private File file;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ArrayList<String> grados= new ArrayList<>();
        grados.add("motocicleta");
        grados.add("auto");
        grados.add("camion");
        grados.add("camioneta");
        cbtipo.setItems(FXCollections.observableArrayList(grados));
        
    }    

    @FXML
    private void irAtras(ActionEvent event) {
        if (user.getRol().equals("Vendedor")){
            try {
                FXMLLoader fxmlloader = App.loadFXMLload("loginVendedorFXML");
                App.setRoot(fxmlloader);
                LoginVendedorFXMLController controller = fxmlloader.getController();
                controller.setUser((Vendedor) user);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        else{
            try {
                FXMLLoader fxmlloader = App.loadFXMLload("LoginAmbos");
                App.setRoot(fxmlloader);
                LoginAmbosController controller = fxmlloader.getController();
                controller.setUser((Ambos)user);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }  
    }
    @FXML
    public static boolean validarRegistroPlaca(String placa){
        String confirmacion="^[a-zA-Z'.\\s]{1,5}$";
        return placa.matches(confirmacion);
    }
    @FXML
    private void registrarVehiculo(ActionEvent event) {
        String op=(String)cbtipo.getValue();
        Pattern p = Pattern.compile("^[a-zA-Z'.\\s]{1,40}$");
        boolean cond = placa.getText().isEmpty()||marca.getText().isEmpty()||modelo.getText().isEmpty()||motor.getText().isEmpty()||anio.getText().isEmpty()||recorrido.getText().isEmpty()||color.getText().isEmpty()||trans.getText().isEmpty()||precio.getText().isEmpty();
        if(!cond){     
            switch (op){
                                       case "auto":
                                           if((validarRegistroPlaca(placa.getText()) && ((Integer.parseInt(anio.getText()))<1900 && Integer.parseInt(anio.getText())>2020))&&(Double.parseDouble(recorrido.getText())<0)){
                                               Auto vn= new Auto(op,placa.getText(),marca.getText(),modelo.getText(),motor.getText(),Integer.parseInt(anio.getText()),Integer.parseInt(recorrido.getText()),color.getText(),combust.getText(),trans.getText(),Integer.parseInt(precio.getText()),4,file.getPath());
                                            vn.registrarVehiculo("Vehiculos.txt");
                                            ArrayList<Vehiculo> autos=Auto.leerRegistroAutos("Vehiculos.text");
                                            Vehiculo.guardarSerializados(autos, "VehiculosSerializados.txt");
                                           }
                                           else if((Integer.parseInt(anio.getText()))<1900 && Integer.parseInt(anio.getText())>2020){
                                               Alert a= new Alert(Alert.AlertType.ERROR,"El año ingresado esta fuera de los limites posibles");
                                               a.show();
                                           }
                                           else if(Double.parseDouble(recorrido.getText())<0){
                                                Alert a= new Alert(Alert.AlertType.ERROR,"El recorrido ingresado esta fuera de los limites posibles");
                                               a.show();
                                           }
                                            
                                            break;
                                        case "camion":
                                            if((validarRegistroPlaca(placa.getText()) && ((Integer.parseInt(anio.getText()))>1900 && Integer.parseInt(anio.getText())<2020))&&(Double.parseDouble(recorrido.getText())>0)){
                                               Auto vn= new Auto(op,placa.getText(),marca.getText(),modelo.getText(),motor.getText(),Integer.parseInt(anio.getText()),Integer.parseInt(recorrido.getText()),color.getText(),combust.getText(),trans.getText(),Integer.parseInt(precio.getText()),4,file.getPath());
                                            vn.registrarVehiculo("Vehiculos.txt");
                                            ArrayList<Vehiculo> autos=Auto.leerRegistroAutos("Vehiculos.text");
                                            Vehiculo.guardarSerializados(autos, "VehiculosSerializados.txt");
                                           }
                                           else if((Integer.parseInt(anio.getText()))<1900 && Integer.parseInt(anio.getText())>2020){
                                               Alert a= new Alert(Alert.AlertType.ERROR,"El año ingresado esta fuera de los limites posibles");
                                               a.show();
                                           }
                                           else if(Double.parseDouble(recorrido.getText())<0){
                                                Alert a= new Alert(Alert.AlertType.ERROR,"El recorrido ingresado esta fuera de los limites posibles");
                                               a.show();
                                           }
                                           
                                            Camion vn1= new Camion(op,placa.getText(),marca.getText(),modelo.getText(),motor.getText(),Integer.parseInt(anio.getText()),Integer.parseInt(recorrido.getText()),color.getText(),combust.getText(),trans.getText(),Integer.parseInt(precio.getText()),4);
                                            vn1.registrarVehiculo("Vehiculos.txt");
                                            ArrayList<Vehiculo> camiones=Auto.leerRegistroAutos("Vehiculos.text");
                                            Vehiculo.guardarSerializados(camiones, "VehiculosSerializados.txt");
                                            
                                            break;
                                        case "camioneta":
                                            if((validarRegistroPlaca(placa.getText()) && ((Integer.parseInt(anio.getText()))>1900 && Integer.parseInt(anio.getText())<2020))&&(Double.parseDouble(recorrido.getText())>0)){
                                               Camioneta vn2= new Camioneta(op,placa.getText(),marca.getText(),modelo.getText(),motor.getText(),Integer.parseInt(anio.getText()),Integer.parseInt(recorrido.getText()),color.getText(),combust.getText(),trans.getText(),Integer.parseInt(precio.getText()),4);
                                            vn2.registrarVehiculo("Vehiculos.txt");
                                            ArrayList<Vehiculo> camionetas=Auto.leerRegistroAutos("Vehiculos.text");
                                            Vehiculo.guardarSerializados(camionetas, "VehiculosSerializados.txt");
                                            vn2.registrarVehiculo("Vehiculos.txt");
                                            ArrayList<Vehiculo> autos=Auto.leerRegistroAutos("Vehiculos.text");
                                            Vehiculo.guardarSerializados(autos, "VehiculosSerializados.txt");
                                           }
                                           else if((Integer.parseInt(anio.getText()))<1900 && Integer.parseInt(anio.getText())>2020){
                                               Alert a= new Alert(Alert.AlertType.ERROR,"El año ingresado esta fuera de los limites posibles");
                                               a.show();
                                           }
                                           else if(Double.parseDouble(recorrido.getText())<0){
                                                Alert a= new Alert(Alert.AlertType.ERROR,"El recorrido ingresado esta fuera de los limites posibles");
                                               a.show();
                                           }                                                                                     
                                            break;
                                        case "motocicleta":
                                            if((validarRegistroPlaca(placa.getText()) && ((Integer.parseInt(anio.getText()))>1900 && Integer.parseInt(anio.getText())<2020))&&(Double.parseDouble(recorrido.getText())>0)){
                                               Motocicleta vn3= new Motocicleta(op,placa.getText(),marca.getText(),modelo.getText(),motor.getText(),Integer.parseInt(anio.getText()),Integer.parseInt(recorrido.getText()),color.getText(),combust.getText(),trans.getText(),Integer.parseInt(precio.getText()),"rio");
                                            vn3.registrarVehiculo("Vehiculos.txt");
                                            ArrayList<Vehiculo> motocicleta=Auto.leerRegistroAutos("Vehiculos.text");
                                            Vehiculo.guardarSerializados(motocicleta, "VehiculosSerializados.txt");
                                           }
                                           else if((Integer.parseInt(anio.getText()))<1900 && Integer.parseInt(anio.getText())>2020){
                                               Alert a= new Alert(Alert.AlertType.ERROR,"El año ingresado esta fuera de los limites posibles");
                                               a.show();
                                           }
                                           else if(Double.parseDouble(recorrido.getText())<0){
                                                Alert a= new Alert(Alert.AlertType.ERROR,"El recorrido ingresado esta fuera de los limites posibles");
                                               a.show();
                                           }                                                                                       
                                            break;
                                        default:
                                            break;
                              }
         if((validarRegistroPlaca(placa.getText()) && ((Integer.parseInt(anio.getText()))>1900 && Integer.parseInt(anio.getText())<2020))&&(Double.parseDouble(recorrido.getText())>0)){
             Alert a= new Alert(Alert.AlertType.INFORMATION,"Se ha registrado con exito su Vehiculo");
              a.show();
         }}else{
             Alert b= new Alert(Alert.AlertType.INFORMATION,"Ha ocurrido un error al registrar el Vehiculo\nCierre esta ventana para ver cual fue el error");
         }}
    @FXML
    private void subirImagen(ActionEvent event) {
      fileChooser  = new FileChooser();
      //new ExtensionFilter("Word File","docx")
      fileChooser.getExtensionFilters().addAll();
      file= fileChooser.showOpenDialog(stage);
      String path = file.getPath();
    }
    
    public void setUser(Usuario u){
        user = u;
    }
    
    public static boolean validarString(String clave){
        String patron = "^([A-Z]?)([a-z]?)\\S{3,}$";
        return clave.matches(patron);
    }
    
    public static boolean validarDecimal(String oferta){
        String patron = "^[0-9]+([.][0-9]+)?$";
        return oferta.matches(patron);
    }
}
