/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.modelo.Ambos;
import ec.edu.espol.modelo.Auto;
import ec.edu.espol.modelo.Comprador;
import ec.edu.espol.modelo.Encriptar;
import ec.edu.espol.modelo.Oferta;
import ec.edu.espol.modelo.Usuario;
import ec.edu.espol.modelo.Vehiculo;
import ec.edu.espol.modelo.Vendedor;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
/**
 * FXML Controller class
 *
 * @author Mile
 */
public class PrincipalController implements Initializable {

    ArrayList<Usuario> usuarios;
    Usuario user;
    
    @FXML
    private GridPane grdPane;
    @FXML
    private TextField correo;
    @FXML
    private PasswordField contrasenia;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Image img = new Image("img/logo.png");
        ImageView imgView = new ImageView(img);
        grdPane.add(imgView, 0, 0);
        imgView.setFitHeight(480);
        imgView.setFitWidth(330);
        usuarios = Usuario.deserializarUsuario("usuarios.ser");
        System.out.println(usuarios);
    }    
    
    @FXML
    private void login(MouseEvent event) {
        String usuario = correo.getText();
        String llave = contrasenia.getText();
        try{
            llave= Encriptar.encriptaHex(Encriptar.to256(llave));
        }catch(NoSuchAlgorithmException e){
            e.getStackTrace();
        }
        try {
            boolean b = Usuario.validarIngreso("usuarios.ser", usuario, llave);
            if(b){
                user = Usuario.buscarUsuario(usuarios, usuario, llave);
                String rol = user.getRol();
                switch(rol){
                    case "Vendedor":
                        FXMLLoader fxmlloaderV = App.loadFXMLload("LoginVendedorFXML");
                        App.setRoot(fxmlloaderV);
                        LoginVendedorFXMLController controllerV = fxmlloaderV.getController();
                        controllerV.setUser((Vendedor)user);
                        break;
                    case "Comprador":
                        FXMLLoader fxmlloaderC = App.loadFXMLload("BusquedaAutoComprador");
                        App.setRoot(fxmlloaderC);
                        BusquedaAutoCompradorController controllerC = fxmlloaderC.getController();
                        controllerC.setUser((Comprador)user);
                        break;
                    case "Ambos":
                        FXMLLoader fxmlloaderA = App.loadFXMLload("LoginAmbos");
                        App.setRoot(fxmlloaderA);
                        LoginAmbosController controllerA = fxmlloaderA.getController();
                        controllerA.setUser((Ambos)user);
                        break;
                    default:
                            break;
                }
            }
            else{
                Alert a = new Alert(Alert.AlertType.ERROR,"Error en las credenciales, intentelo nuevamente");
                a.show(); 
            }
        } catch (Exception ex) {
            Alert a = new Alert(Alert.AlertType.ERROR,"Usuario no registrado, intentelo nuevamente");
            a.show();
        }
    }

    @FXML
    private void registro(MouseEvent event) {
        try {
            App.setRoot("Registrar");
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR,"Cambio de escena fallido");
            a.show();
        }
        
    }

}
