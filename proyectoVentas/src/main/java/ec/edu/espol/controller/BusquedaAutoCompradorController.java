/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.modelo.Usuario;
import ec.edu.espol.modelo.Vehiculo;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
//import java.util.

/**
 * FXML Controller class
 *
 * @author macbookpro
 */
public class BusquedaAutoCompradorController implements Initializable {
    Usuario user;
    ArrayList<Vehiculo> vehiculos;
    @FXML
    private ToggleGroup grupoRadioBotones;
    @FXML
    private ComboBox cbxTipo;
    @FXML
    private ComboBox cbxKm;
    @FXML
    private ComboBox cbxPrecio;
    @FXML
    private ComboBox cbxAnio;
    @FXML
    private AnchorPane scrollinfoV;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vehiculos = Vehiculo.deserializarVehiculos("vehiculos.ser");
        ArrayList<String> tipo = Vehiculo.filtrarTipos(vehiculos);
        ArrayList<Double> recorrido = Vehiculo.filtrarKm(vehiculos);
        ArrayList<Double> precio = Vehiculo.filtrarPrecio(vehiculos);
        ArrayList<Integer> year = Vehiculo.filtrarYear(vehiculos);
        cbxTipo.setItems(FXCollections.observableArrayList(tipo));
        cbxKm.setItems(FXCollections.observableArrayList(recorrido));
        cbxPrecio.setItems(FXCollections.observableArrayList(precio));
        cbxAnio.setItems(FXCollections.observableArrayList(year));
    }    


    @FXML
    private void buscarAuto(MouseEvent event) {
        ArrayList<String> parametros = new ArrayList<>();
        String tipo = (String) cbxTipo.getValue();
        String km = (String) cbxKm.getValue();
        String precio = (String) cbxPrecio.getValue();
        String year = (String) cbxAnio.getValue();
        ArrayList<Vehiculo> filtrados = Vehiculo.buscarVehiculos(vehiculos, parametros);
        VBox vbx = new VBox();
        for (Vehiculo v: filtrados){
            RadioButton rd1= new RadioButton(v.toString());
            rd1.setToggleGroup(grupoRadioBotones);
            vbx.getChildren().add(rd1);
        }
    }

    @FXML
    private void visitarPerfil(MouseEvent event) {
        try {
            FXMLLoader fxmlloader = App.loadFXMLload("PerfilFXML");
            App.setRoot(fxmlloader);
            PerfilFXMLController controller = fxmlloader.getController();
            controller.setUser(user);
        } catch (IOException ex) {
            ex.printStackTrace();
        } 
    }

    @FXML
    private void OfertarComp(MouseEvent event) {
        RadioButton select= (RadioButton)grupoRadioBotones.getSelectedToggle();
        String atributos = select.getText();
        try {
            FXMLLoader fxmlloader = App.loadFXMLload("OfertarComprador");
            App.setRoot(fxmlloader);
            OfertarCompradorController controller = fxmlloader.getController();
            controller.setUser(user, atributos);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void salir(MouseEvent event) {
        try {
            App.setRoot("Principal");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void setUser(Usuario u){
        user=u;
    }
}
